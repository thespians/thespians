/*
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://sam.zoy.org/wtfpl/COPYING for more details.
 *
*/

#ifndef HYPOKRITES_ACTOR_HH
#define HYPOKRITES_ACTOR_HH

#include <boost/shared_ptr.hpp>
#include <boost/make_shared.hpp>
#include <boost/thread/thread.hpp>
#include <boost/thread/mutex.hpp>
#include <boost/thread/condition_variable.hpp>
#include <boost/noncopyable.hpp>

#include <list>

namespace hypokrites
{

	template <class ActorImpl>
	class ActorFactory : public ActorImpl
	{

		public:

		template <typename... Params>
		ActorFactory(Params&&... params) : ActorImpl(std::forward<Params>(params)...), thread_(&ActorImpl::run, this)
		{
		}

		virtual ~ActorFactory()
		{
			thread_.join();
		}

		private:

		virtual void use_ActorFactory_to_instanciate_actors()
		{
		}

		boost::thread thread_;

	};

	template <class Msg>
	class Actor : public boost::noncopyable
	{

		public:

		typedef boost::shared_ptr<Msg> MsgPtr;

		typedef std::list<MsgPtr> Msgs;
		typedef boost::shared_ptr<Msgs> MsgsPtr;

		typedef typename Msgs::iterator MsgsIterator;

		Actor()
		{
			inbox_ = boost::make_shared<Msgs>();
		}

		virtual ~Actor()
		{
		}

		void send_msg(MsgPtr msg)
		{
			boost::mutex::scoped_lock lock(inbox_mutex_);
			send_msg_locked(lock, msg);
		}

		MsgsPtr recv()
		{
			boost::mutex::scoped_lock lock(inbox_mutex_);
			return recv_locked(lock);
		}

		virtual void run()
		{
			bool need_exit = false;
			while (!need_exit)
			{
				MsgsPtr msgs = recv();
				need_exit = dispatch(msgs);
			}
		}

		virtual bool dispatch(MsgsPtr msgs)
		{
			bool need_exit = false;
			for (MsgsIterator msgs_iterator = msgs->begin(); msgs_iterator != msgs->end(); ++msgs_iterator)
				need_exit |= handle_msg(*msgs_iterator);
			return need_exit;
		}

		virtual bool handle_msg(MsgPtr) = 0;

		protected:

		static const bool should_continue = false;
		static const bool should_exit = true;

		private:
		
		virtual void use_ActorFactory_to_instanciate_actors() = 0;

		template <class Lock>
		void send_msg_locked(Lock&, MsgPtr msg)
		{
			inbox_->push_back(msg);
			inbox_cond_.notify_one();
		}

		template <class Lock>
		MsgsPtr recv_locked(Lock& lock)
		{
			while (inbox_->empty())
				inbox_cond_.wait(lock);
			MsgsPtr list = inbox_;
			inbox_ = boost::make_shared<Msgs>();
			return list;
		}

		MsgsPtr inbox_;
		boost::mutex inbox_mutex_;
		boost::condition_variable inbox_cond_;

	};

}

#endif
