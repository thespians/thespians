/*
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://sam.zoy.org/wtfpl/COPYING for more details.
 *
*/

#ifndef THESPIANS_ACTOR_HH
#define THESPIANS_ACTOR_HH

#include <hypokrites/actor.hh>

#include <boost/function.hpp>
#include <boost/bind.hpp>

namespace thespians
{

	typedef boost::function<bool ()> Msg;

	template <class ActorImpl>
	class ActorFactory : public hypokrites::ActorFactory<ActorImpl>
	{

		public:

		static const bool wait_exit = false;
		static const bool send_exit_msg = true;

		ActorFactory() : should_send_exit_(send_exit_msg)
		{
		}

		template <typename... Params>
		ActorFactory(bool should_send_exit, Params&&... params) : hypokrites::ActorFactory<ActorImpl>(std::forward<Params>(params)...), should_send_exit_(should_send_exit)
		{
		}

		virtual ~ActorFactory()
		{
			if (should_send_exit_)
			{
				this->send_exit();
			}
		};

		private:

		bool should_send_exit_;

	};

	class Actor : public hypokrites::Actor<Msg>
	{

		template <typename F>
		class Ack
		{

			public:

			Ack(Actor* a, F&& f) : a_(a)
			{
				f_ = boost::make_shared<F>(f);
			}

			template <typename... Params>
			void operator()(Params&&... params)
			{
				MsgPtr msg = boost::make_shared<Msg>(boost::bind(&Actor::ack<boost::shared_ptr<F>, Params...>, a_, f_, std::forward<Params>(params)...));
				a_->send_msg(msg);
			}

			private:

			boost::shared_ptr<F> f_;
			Actor* a_;

		};

		public:

		Actor()
		{
		}

		virtual bool handle_msg(MsgPtr msg)
		{
			return (*msg)();
		}

		template <typename... Params>
		void send(Params&&... params)
		{
			MsgPtr msg = boost::make_shared<Msg>(boost::bind(std::forward<Params>(params)...));
			send_msg(msg);
		}

		void send_exit()
		{
			send(&Actor::handle_exit, this);
		}

		template <typename... Params>
		auto create_ack(Params&&... params)
    #define create_ack_f boost::bind(std::forward<Params>(params)...)
		-> Ack<decltype(create_ack_f)>
		{
			return Ack<decltype(create_ack_f)>(this, create_ack_f);
		}

		protected:

		template <typename AckPtr, typename... Params>
		void reply(AckPtr ack, Params&&... params)
		{
			ack(std::forward<Params>(params)...);
		}

		bool handle_exit()
		{
			return hypokrites::Actor<Msg>::should_exit;
		}

		private:

		template <typename AckPtr, typename... Params>
		bool ack(AckPtr ack, Params... params)
		{
			(*ack)(std::forward<Params>(params)...);
		}

	};

}

#endif
