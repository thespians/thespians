/*
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://sam.zoy.org/wtfpl/COPYING for more details.
 *
*/

#ifndef DET_HH
#define DET_HH

#include <thespians/actor.hh>

#include <vector>

template <int N>
struct DetActorHandleDet
{
	typedef bool result_type;

	template <class ActorImpl, class Matrix, class AckPtr>
	result_type operator()(ActorImpl* actor, const Matrix& m, AckPtr ack)
	{
		typedef thespians::ActorFactory<ActorImpl> Actor;
		if (m.size1() == 1 && m.size2() == 1)
		{
			actor->reply(ack, m(0, 0));
		}
		else
		{
			boost::shared_ptr<std::vector<typename Matrix::value_type>> dets = boost::make_shared<std::vector<typename Matrix::value_type>>(m.size1());
			boost::shared_ptr<typename Matrix::size_type> det_number = boost::make_shared<typename Matrix::size_type>();
			boost::shared_ptr<std::vector<Actor>> actors = boost::make_shared<std::vector<Actor>>(m.size1());
			for (typename Matrix::size_type i = 0; i < m.size1(); ++i)
			{
				boost::shared_ptr<Matrix> mp = boost::make_shared<Matrix>(m.size1() - 1, m.size2() - 1);
				for (typename Matrix::size_type j = 0; j < i; ++j)
					for (typename Matrix::size_type k = 1; k < m.size2(); ++k)
						(*mp)(j, k - 1) = m(j, k);
				for (typename Matrix::size_type j = i + 1; j < m.size1(); ++j)
					for (typename Matrix::size_type k = 1; k < m.size2(); ++k)
						(*mp)(j - 1, k - 1) = m(j, k);
				(*actors)[i].template send_det<N - 1>(*mp, actor->create_ack(&ActorImpl::template handle_det_reply<Matrix, AckPtr>, actor, mp, ack, actors, dets, det_number, i, _1));
			}
		}
		return ActorImpl::should_continue;
	}
};

template <>
struct DetActorHandleDet<0>
{
	typedef bool result_type;

	template <class ActorImpl, class Matrix, class AckPtr>
	result_type operator()(ActorImpl* actor, const Matrix& m, AckPtr ack)
	{
		return ActorImpl::should_continue;
	}
};

class DetActorImpl : public thespians::Actor
{

	public:

	template <int N = 12, class Matrix, class AckPtr>
	void send_det(const Matrix& m, AckPtr ack)
	{
		send(DetActorHandleDet<N>(), this, boost::cref(m), ack);
	}

	private:

	typedef thespians::ActorFactory<DetActorImpl> DetActor;

	template <class Matrix, class AckPtr>
	bool handle_det(const Matrix& m, AckPtr ack)
	{
		return thespians::Actor::should_continue;
	}

	template <class Matrix, class AckPtr>
	bool handle_det_reply(boost::shared_ptr<Matrix> m, AckPtr ack, boost::shared_ptr<std::vector<DetActor>> actors, boost::shared_ptr<std::vector<typename Matrix::value_type>> dets, boost::shared_ptr<typename Matrix::size_type> det_number, typename Matrix::size_type index, typename Matrix::value_type det)
	{
		(*dets)[index] = det;
		(*det_number)++;
		if (*det_number == dets->size())
		{
			actors->empty();
			typename Matrix::value_type m_det = 0;
			bool sign = true;
			for (typename Matrix::size_type i = 0; i < m->size1(); ++i)
			{
				m_det += (sign ? 1 : -1) * (*m)(i, 0) * (*dets)[i];
				sign = !sign;
			}
			reply(ack, m_det);
		}
		return thespians::Actor::should_continue;
	}

	template <int>
	friend class DetActorHandleDet;

};

typedef thespians::ActorFactory<DetActorImpl> DetActor;

#endif
