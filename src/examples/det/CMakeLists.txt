pkg_check_modules(THESPIANS REQUIRED thespians)

add_executable(det main.cc det.hh)

include_directories(${THESPIANS_INCLUDE_DIRS})
set_target_properties(det PROPERTIES COMPILE_FLAGS ${THESPIANS_CFLAGS_OTHER} LINK_FLAGS ${THESPIANS_LDFLAGS})
