/*
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://sam.zoy.org/wtfpl/COPYING for more details.
 *
*/

#include "det.hh"

#include <thespians/actor.hh>

#include <boost/numeric/ublas/matrix.hpp>
#include <boost/numeric/ublas/io.hpp>
#include <boost/random/mersenne_twister.hpp>
#include <boost/random/uniform_int.hpp>
#include <boost/random/variate_generator.hpp>

#include <iostream>

class MainActorImpl : public thespians::Actor
{

	public:

	typedef int scalar_type;

	void run()
	{
		boost::numeric::ublas::matrix<scalar_type> m(7, 7);
		fill_matrix_random(m);
		compute_det(m);
		thespians::Actor::run();
	}

	private:

	template <class Matrix>
	void fill_matrix_random(Matrix& m)
	{
		boost::mt19937 generator(static_cast<unsigned int>(std::time(0)));
		boost::uniform_int<> uni_dist;
		boost::variate_generator<boost::mt19937&, boost::uniform_int<> > uni(generator, uni_dist);
		for (unsigned int i = 0; i < m.size1 (); ++i)
		  for (unsigned int j = 0; j < m.size2 (); ++j)
		    m(i, j) = uni();
	}

	template <class Matrix>
	void compute_det(const Matrix& m)
	{
		det_actor_.send_det(m, create_ack(&MainActorImpl::handle_det_reply<Matrix>, this, boost::cref(m), _1));
	}

	template <class Matrix>
	bool handle_det_reply(const Matrix& m, scalar_type det)
	{
		std::cout << "Matrix is " << m << "\n";
		std::cout << "Det is " << det << "\n";
		return thespians::Actor::should_exit;
	}

	DetActor det_actor_;

};

typedef thespians::ActorFactory<MainActorImpl> MainActor;

int main()
{
	MainActor main_actor(MainActor::wait_exit);
}
