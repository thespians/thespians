/*
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://sam.zoy.org/wtfpl/COPYING for more details.
 *
*/

#include "echo.hh"

#include <thespians/actor.hh>

#include <iostream>

class MainActorImpl : public thespians::Actor
{

	public:

	void run()
	{
		echo_actor_.send_echo(42, create_ack(&MainActorImpl::handle_echo_reply_1, this, _1));
		thespians::Actor::run();
	}

	bool handle_echo_reply_1(int i)
	{
		std::cout << "Main actor received " << i << " from echo actor\n";
		echo_actor_.send_echo(std::string("youhou"), create_ack(&MainActorImpl::handle_echo_reply_2, this, _1));
		return thespians::Actor::should_continue;
	}

	bool handle_echo_reply_2(const std::string& s)
	{
		std::cout << "Main actor received " << s << " from echo actor\n";
		echo_actor_.send_echo(2.5, create_ack(&MainActorImpl::handle_echo_reply_3, this, 42, _1));
		return thespians::Actor::should_continue;
	}

	bool handle_echo_reply_3(int i, float j)
	{
		std::cout << "Main actor received " << i << " from bounded argument\n";
		std::cout << "Main actor received " << j << " from echo actor\n";
		return thespians::Actor::should_exit;
	}

	private:

	EchoActor echo_actor_;

};

typedef thespians::ActorFactory<MainActorImpl> MainActor;

int main()
{
	MainActor main_actor(MainActor::wait_exit);
}
