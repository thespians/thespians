/*
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://sam.zoy.org/wtfpl/COPYING for more details.
 *
*/

#ifndef ECHO_HH
#define ECHO_HH

#include <thespians/actor.hh>

#include <iostream>

class EchoActorImpl : public thespians::Actor
{

	public:

	template <typename T, class AckPtr>
	void send_echo(T&& t, AckPtr ack)
	{
		std::cout << "Sending " << t << " to echo actor\n";
		send(&EchoActorImpl::handle_echo<T, AckPtr>, this, t, ack);
	}

	private:

	template <typename T, class AckPtr>
	bool handle_echo(T t, AckPtr ack)
	{
		std::cout << "Echo actor received " << t << "\n";
		reply(ack, t);
		return thespians::Actor::should_continue;
	}

};

typedef thespians::ActorFactory<EchoActorImpl> EchoActor;

#endif
