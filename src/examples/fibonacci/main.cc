/*
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://sam.zoy.org/wtfpl/COPYING for more details.
 *
*/

#include "fibonacci.hh"

#include <thespians/actor.hh>
#include <boost/circular_buffer.hpp>
#include <boost/tuple/tuple.hpp>

#include <iostream>
#include <vector>

class MainActorImpl : public thespians::Actor
{

	public:

	typedef uint64_t size_type;

	MainActorImpl(size_type nb_terms, size_type u0, size_type u1) : nb_terms_(nb_terms), terms_(nb_terms_)
	{
		terms_[0] = u0;
		terms_[1] = u1;
	}

	void run()
	{
		handle_fibonacci_reply(terms_[1], 1);
		thespians::Actor::run();
	}

	bool handle_fibonacci_reply(size_type term, size_type term_index)
	{
		terms_[term_index] = term;
		if (term_index + 1 < nb_terms_)
		{
			fibonacci_actor_.send_fibonacci(terms_[term_index - 1], terms_[term_index], create_ack(&MainActorImpl::handle_fibonacci_reply, this, _1, term_index + 1));
			return thespians::Actor::should_continue;
		}
		else
		{
			show_terms();
			return thespians::Actor::should_exit;
		}
	}

	private:

	void show_terms()
	{
		for (auto i = terms_.begin(); i != terms_.end(); ++i)
			std::cout << *i << '\n';
	}

	FibonacciActor fibonacci_actor_;
	size_type nb_terms_;
	std::vector<size_type> terms_;

};

typedef thespians::ActorFactory<MainActorImpl> MainActor;

int main()
{
	MainActor main_actor(MainActor::wait_exit, 40, 0, 1);
}
