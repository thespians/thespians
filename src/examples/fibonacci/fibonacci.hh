/*
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://sam.zoy.org/wtfpl/COPYING for more details.
 *
*/

#ifndef FIBONACCI_HH
#define FIBONACCI_HH

#include <thespians/actor.hh>

class FibonacciActorImpl : public thespians::Actor
{

	public:

	template <class AckPtr>
	void send_fibonacci(int u0, int u1, AckPtr ack)
	{
		send(&FibonacciActorImpl::handle_fibonacci<AckPtr>, this, u0, u1, ack);
	}

	private:

	template <class AckPtr>
	bool handle_fibonacci(int u0, int u1, AckPtr ack)
	{
		reply(ack, u0 + u1);
		return thespians::Actor::should_continue;
	}

};

typedef thespians::ActorFactory<FibonacciActorImpl> FibonacciActor;

#endif
